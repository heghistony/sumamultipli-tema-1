﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercitiulNr2BUN
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduceti valoarea lui <<N>> ");
            bool rezn = int.TryParse(Console.ReadLine(), out int n);
            if (!rezn)
            {
                Console.WriteLine("Ati introdus un numar gresit! ");
                return;
            }

            Console.WriteLine("Introduceti valoarea lui <<M>> ");
            bool rezm = int.TryParse(Console.ReadLine(), out int m);
            if (!rezm)
            {
                Console.WriteLine("Ati introdus un numar gresit! ");
                return;
            }
            Console.WriteLine($"Suma multiplilor este: {SumaMultipli(n, m)} !");

            Console.ReadKey();
        }
        static int SumaMultipli(int n, int m)
        {
            int suma = 0;
            for (int i = 0; i < m; i++)
            {
                if (i % n == 0)
                {
                    suma = suma + i;
                }
            }
            return suma;
        }
    }
}